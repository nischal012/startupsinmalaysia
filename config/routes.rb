Rails.application.routes.draw do

	devise_for :users
	root to: 'infos#index'
	get 'home', to: 'infos#index'
	get 'dashboard', to: 'infos#dashboard', as: 'dashboard'
	resources :infos, except: [:delete,:update]
	resources :addstartups, only: [:new,:create]

end
