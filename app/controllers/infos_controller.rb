class InfosController < ApplicationController
	before_action :authenticate_user!, only: [:new,:admin]
	def new
		@info=Info.new
	end

	def create
		@info=Info.create(info_params)
		if @info.save 
			redirect_to infos_path
		else 
			render new_info_path
		end
		

	end

	def index 
		@info=Info.all.paginate(page: params[:page], per_page:12)
		@this_month=Info.where(:created_at => Time.now.beginning_of_month..Time.now.end_of_month)
	end


	def dashboard
	end

	def show
		@info=Info.find(params[:id])
	end


	private 
	def info_params
		params.require(:info).permit(:title,:founders,:description,:image)
	end


end
